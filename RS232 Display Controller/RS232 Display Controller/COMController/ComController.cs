﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Collections;
using System.Diagnostics;

using RS232_Display_Controller.CommandBuilder;
using RS232_Display_Controller.Classes;

namespace RS232_Display_Controller.COMController
{
    public class ComController
    {
        public delegate void LogDelegate(string message);
        public delegate void ResponseReceivedDelegate(CommandResponse response);
        public enum ScreenType
        {
            NEC, SAMSUNG, MITSUBISHI, ESPRIT
        }
        public enum Commands
        {
            POWER_ON, POWER_OFF,
            INPUT_VGA, INPUT_RGB, INPUT_DVI, INPUT_HDMI, INPUT_OPTION, INPUT_DP,
            STATUS, SERIAL
        }

        private ScreenType screenType;
        private ScreenCommandBuilder builder;
        public SerialPort Port {
            get {
                return port;
            }
        }
        private SerialPort port;
        private LogDelegate logger;

        public bool Status 
        { 
            get 
            { 
                bool open = (port != null && port.IsOpen);
                if (!open && port != null)
                {
                    try
                    {
                        lock (port)
                        {
                            //Open port to test if available
                            port.Open();
                            if (port.IsOpen)
                            {
                                //Close again incase we don't use it
                                port.Close();
                                return true;
                            }
                            else return false;
                        }
                    }
                    catch
                    {
                        return false;
                    }
                }
                else if (port != null)
                {
                    return open;
                }
                else
                {
                    return false;
                }
            } 
        }

        public ComController(string portName, ScreenType screenType, LogDelegate logger)
        {
            this.screenType = screenType;
            switch (screenType)
            {
                case ScreenType.NEC:
                    builder = new NECScreenCommandBuilder();
                    break;
                case ScreenType.SAMSUNG:
                    builder = new SamsungScreenCommandBuilder();
                    break;
                case ScreenType.MITSUBISHI:
                    builder = new MitsubishiScreenCommandBuilder();
                    break;
                case ScreenType.ESPRIT:
                    builder = new EspritScreenCommandBuilder();
                    break;
                default:
                    logger("Unknown manufacturer, cannot continue!");
                    return;
            }

            SerialPortSpec spec = builder.GetSerialPortSpec();
            
            port = new SerialPort(portName, spec.BaudRate, spec.Parity, spec.DataBits, spec.StopBits);
            port.ReadTimeout = 10000;
            lock (port)
            {
                port.Handshake = spec.FlowControl;
                
                this.logger = logger;

                try
                {
                    port.Open();
                    logger("Prepared " + portName + " for " + screenType.ToString() + " (BaudRate: " + spec.BaudRate +
                           ", Parity: " + spec.Parity + ", DataBits: " + spec.DataBits + ", StopBits: " + spec.StopBits +
                           ", Handshake: " + spec.FlowControl + ")");
                    port.Close();
                }
                catch (Exception e)
                {
                    logger("Unable to open " + portName + ": " + e.GetType().Name + " - " + e.Message);
                }
            }
        }

        public void InitializeCommands()
        {
            try
            {
                builder.InitRSData();

                if(RSMain.GetDataFromFile() == "")
                {
                    RSMain.WriteDataToFile(RSMain.SerializeJSONToString(builder.GetRSData()));
                }
                
            }
            catch(Exception e)
            {
                logger("InitializeCommands Exception: " + e.Message);
            }
        }

        public void Dispose()
        {
            try
            {
                lock (port)
                {
                    if (port != null)
                    {
                        if (port.IsOpen)
                        {
                            port.Close();
                        }

                        port.Dispose();
                        logger("Port closed");
                    }
                }
            }
            catch (Exception e)
            {
                logger ("Error disposing of serial port: " + e.Message);
            }
        }

        public String BuildCommand(Commands command, string destinationId)
        {
            String commandStr = builder.BuildCommand(command, destinationId);
            
            return commandStr;
        }
        public String BuildCommand(string command, string destinationId)
        {
            String commandStr = builder.BuildCommand(command, destinationId);
            return commandStr;
        }
        public String BuildCommandNew(string command, string destinationId)
        {
            String commandStr = builder.BuildCommandNew(command, destinationId, true);
            return commandStr;
        }

        public bool SendCommand(Commands command, string destinationId, ResponseReceivedDelegate responseCallback, ref string error)
        {
            String commandStr = BuildCommand(command, destinationId);

            return SendCommand(commandStr, responseCallback, ref error);
        }
        public bool SendCommand(string command, string destinationId, ResponseReceivedDelegate responseCallback, ref string error)
        {
            String commandStr = BuildCommand(command, destinationId);
            return SendCommand(commandStr, responseCallback, ref error);
        }

        private bool SendCommand(string builtCommand, ResponseReceivedDelegate responseCallback, ref string error)
        {
            bool success = SendCommand(builtCommand, responseCallback != null, ref error);

            if (responseCallback != null)
            {
                Thread responseThread = new Thread(new ParameterizedThreadStart(thread_ReadResponse));
                responseThread.Name = "Response Listener";
                responseThread.Start(responseCallback);
            }

            return success;
        }
        private bool SendCommand(string command, bool waitForResponse, ref string error)
        {
            if (command != null)
            {
                try
                {
                    lock (port)
                    {
                        try
                        {
                            if (!port.IsOpen)
                            {
                                port.Open();
                                logger("Port opened");
                            }
                            byte[] msg = HexToByte(command);

                            logger("Sent " + command);
                            port.Write(msg, 0, msg.Length);

                            if (!waitForResponse)
                            {
                                port.Close();
                                logger("Port Closed");
                            }
                            return true;
                        }
                        catch (Exception e)
                        {
                            if (error != null)
                            {
                                error = "Error: " + e.GetType().Name + " - " + e.Message;
                            }
                            logger("Error: " + e.GetType().Name + " - " + e.Message);
                            try
                            {
                                port.Close();
                                logger("Port Closed");
                            }
                            catch { }
                            return false;
                        }
                    }
                }
                catch(Exception e)
                {
                    logger("Unable to send command! Exception: "+e.Message);
                }
                return false;
            }
            else
            {
                logger("Error: Cannot send command, No command passed");
                return false;
            }
        }

        private bool SendCommandNew(string builtCommand, ResponseReceivedDelegate responseCallback, ref string error)
        {
            bool success = SendCommand(builtCommand, responseCallback != null, ref error);

            if (responseCallback != null)
            {
                Thread responseThread = new Thread(new ParameterizedThreadStart(thread_ReadResponseNew));
                responseThread.Name = "Response Listener";
                responseThread.Start(responseCallback);
            }

            return success;
        }

        public bool SendCommandNew(string command, string destinationId, ResponseReceivedDelegate responseCallback, ref string error)
        {
            String commandStr = BuildCommandNew(command, destinationId);
            return SendCommandNew(commandStr, responseCallback, ref error);
        }

        public bool SendCompleteCommand(string commmand, ResponseReceivedDelegate responseCallback, ref string error)
        {
            bool success = false;
            try
            {
                success = SendCommand(commmand, responseCallback != null, ref error);

                if (responseCallback != null)
                {
                    Thread responseThread = new Thread(new ParameterizedThreadStart(thread_ReadResponseNew));
                    responseThread.Name = "Response Listener";
                    responseThread.Start(responseCallback);
                }
            }
            catch(Exception e)
            {
                logger("Unable to Send Complete Command!");
            }
            return success;
        }

        private void thread_ReadResponse(object param)
        {
            ResponseReceivedDelegate callback = (ResponseReceivedDelegate)param;

            lock (port)
            {
                if (!port.IsOpen)
                {
                    return;
                    port.Open();
                    logger("Port opened");
                }

                port.ReadTimeout = 10000;
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                

                while (port.IsOpen && port.BytesToRead <= 0 && stopwatch.ElapsedMilliseconds < port.ReadTimeout)
                {
                    Thread.Sleep(200);
                }

                if (port.IsOpen && port.BytesToRead > 0)
                {
                    byte[] buff = new byte[port.BytesToRead];
                    port.Read(buff, 0, buff.Length);
                    
                    CommandResponse response = builder.BuildResponse(buff);
                    //logger("response = " + response.data);
                    callback(response);
                }
                else
                {
                    logger("No response read");
                    callback(null);
                }

                try
                {
                    port.Close();
                    logger("Port Closed");
                }
                catch
                {
                }
            }
        }

        private void thread_ReadResponseNew(object param)
        {
            ResponseReceivedDelegate callback = (ResponseReceivedDelegate)param;

            try
            {
                lock (port)
                {
                    if (!port.IsOpen)
                    {
                        port.Open();
                        logger("Port opened");
                    }

                    port.ReadTimeout = 3000;
                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();

                    while (port.IsOpen && port.BytesToRead <= 0 && stopwatch.ElapsedMilliseconds < port.ReadTimeout)
                    {
                        Thread.Sleep(200);
                    }

                    if (port.IsOpen && port.BytesToRead > 0)
                    {
                        byte[] buff = new byte[port.BytesToRead];
                        port.Read(buff, 0, buff.Length);

                        CommandResponse response = builder.BuildResponseNew(buff);
                        callback(response);
                    }
                    else
                    {
                        logger("No response read");
                        callback(null);
                    }

                    try
                    {
                        port.Close();
                        logger("Port Closed");
                    }
                    catch
                    {
                    }
                }
            }
            catch(Exception e)
            {
                logger("ComController - thread_ReadResponse Exception: " + e.Message);
            }
        }

        public static byte[] HexToByte(string command)
        {
            if (command.Length % 2 == 0)
            {
                command = command.Replace(" ", "");

                byte[] comBuffer = new byte[command.Length / 2];
                for (int i = 0; i < command.Length - 1; i += 2)
                {
                    comBuffer[i / 2] = Convert.ToByte(command.Substring(i, 2), 16);
                }

                return comBuffer;
            }
            else 
            {
                throw new Exception("Invalid command");
            }
        }

        public ScreenCommandBuilder GetScreenBuilder()
        {
            return builder;
        }
    }
}
