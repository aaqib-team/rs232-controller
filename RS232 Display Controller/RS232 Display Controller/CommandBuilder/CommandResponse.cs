﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RS232_Display_Controller.CommandBuilder
{
    public class CommandResponse
    {
        public bool success { get; set; }
        public int errorCode { get; set; }
        public string data { get; set; }
        public byte[] dataBytes { get; set; }

        public override string ToString()
        {
            String ret = ((success) ? "Command Successful" : "Command Failed") + ": ";
            ret += ((success) ? data : errorCode.ToString());

            return ret;
        }
    }
}
