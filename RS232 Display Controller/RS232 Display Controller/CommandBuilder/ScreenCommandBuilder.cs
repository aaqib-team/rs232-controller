﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RS232_Display_Controller.COMController;
using RS232_Display_Controller.Classes;

namespace RS232_Display_Controller.CommandBuilder
{
    public abstract class ScreenCommandBuilder
    {
        protected Dictionary<ComController.Commands, string> commandValues = new Dictionary<ComController.Commands, string>();
        protected Dictionary<string, string> screenIds = new Dictionary<string, string>();

        public string BuildCommand(ComController.Commands command, string destinationId)
        {
            return BuildCommand(command, destinationId, true);
        }
        public string BuildCommand(string command, string destinationId)
        {
            return BuildCommand(command, destinationId, true);
        }
        /// <summary>
        /// Builds a string representation of the command to send, depending on the builders implementation
        /// </summary>
        /// <param name="command">The command to execute</param>
        /// <param name="destination">The screen ID to send the message to</param>
        /// <returns>The command to be sent to the port</returns>
        public abstract string BuildCommand(ComController.Commands command, string destinationId, bool stripSpaces);
        public abstract string BuildCommand(string command, string destinationId, bool stripSpaces);

        public abstract string BuildCommandNew(string command, string destinationId, bool stripSpaces);


        public abstract SerialPortSpec GetSerialPortSpec();

        protected abstract string CalculateCheckCode(string messageBody);

        public abstract CommandResponse BuildResponse(byte[] buff);

        public abstract CommandResponse BuildResponseNew(byte[] buff);

        /// <summary>
        /// Build Params Data. Converts data to hex. Stored in 'paramsData'.
        /// </summary>
        /// <param name="args"> Arguments/Parameters sent along with the command. E.g For Brightness. </param>
        public abstract void BuildParamsData(List<string> args);

        /// <summary>
        /// Build command with params data. Each Screen type will build it's parameters accordingly.
        /// </summary>
        /// <param name="currentMessage"> Command being sent to the screen. </param>
        /// <param name="paramsData"> Parameters. Might be Encoded in hexadecimal. E.g Brightness is set as '10' then this would be converted to hex if the display type is NEC.</param>
        /// <returns></returns>
        public abstract string BuildCommandWithParamsData(string currentMessage, string paramsData);

        /// <summary>
        /// Set Default Data as type RSFormat.
        /// </summary>
        public abstract void InitRSData();

        public abstract List<RSFormat> GetRSData();

        public abstract string GetParamsData();

    }
}
