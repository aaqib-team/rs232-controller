﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading.Tasks;
using RS232_Display_Controller.COMController;
using RS232_Display_Controller.Classes;

namespace RS232_Display_Controller.CommandBuilder
{
    public class SamsungScreenCommandBuilder : ScreenCommandBuilder
    {
        private static readonly string HEAD = "AA";

        private static readonly SerialPortSpec portSpec = new SerialPortSpec(9600, Parity.None, 8, StopBits.One, Handshake.None);

        private string paramsData = "";

        public SamsungScreenCommandBuilder()
        {
            //Set up commands for this builder
            //Any instances of STX will be replaced by the constant above later
            commandValues.Add(COMController.ComController.Commands.POWER_OFF, "11 ID 01 00");
            commandValues.Add(COMController.ComController.Commands.POWER_ON, "11 ID 01 01");

            commandValues.Add(COMController.ComController.Commands.INPUT_VGA, "14 ID 01 14");
            commandValues.Add(COMController.ComController.Commands.INPUT_RGB, "14 ID 01 08");
            commandValues.Add(COMController.ComController.Commands.INPUT_DVI, "14 ID 01 18");
            commandValues.Add(COMController.ComController.Commands.INPUT_HDMI, "14 ID 01 21");

            commandValues.Add(ComController.Commands.STATUS, "00 ID 00");
            commandValues.Add(ComController.Commands.SERIAL, "0B ID 00");

            //Set up screen IDs for this builder
            for (int i = 1; i <= 99; i++)
            {
                string hexDigit = i.ToString("X");
                if (hexDigit.Length == 1) hexDigit = "0" + hexDigit;
                screenIds.Add(i.ToString(), hexDigit);
            }
            screenIds.Add("*", "FE");
            screenIds.Add("0", "FF");
        }

        public override SerialPortSpec GetSerialPortSpec()
        {
            return portSpec;
        }

        public override string BuildCommand(string command, string destinationId, bool stripSpaces)
        {
            throw new InvalidOperationException("String commands not yet implemented for Samsung devices");
        }
        public override string BuildCommand(COMController.ComController.Commands command, string destinationId, bool stripSpaces)
        {
            String commandValue = null;
            string destination = "";
            if (!screenIds.TryGetValue(destinationId, out destination))
            {
                destination = screenIds["*"];
            }

            bool validCommand = commandValues.TryGetValue(command, out commandValue);

            if (validCommand)
            {
                StringBuilder commandSB = new StringBuilder();

                //Build command
                commandSB.Append(HEAD + " ");
                commandSB.Append(commandValue.Replace("ID", destination) + " ");
                //Calculate the check sum with all the message so far minus the starting SOH
                commandSB.Append(CalculateCheckCode(commandSB.ToString().Replace(HEAD, "")));

                //Remove spaces from string
                if (stripSpaces) commandSB.Replace(" ", "");

                return commandSB.ToString();
            }
            else
            {
                throw new InvalidOperationException("This command is not available on this device");
            }
        }

        public override string BuildCommandNew(string command, string destinationId, bool stripSpaces)
        {
            String commandValue = null;
            string destination = "";
            if (!screenIds.TryGetValue(destinationId, out destination))
            {
                destination = screenIds["*"];
            }

            //bool validCommand = commandValues.TryGetValue(command, out commandValue);

            if (command != "")
            {
                StringBuilder commandSB = new StringBuilder();

                //Build command
                commandSB.Append(HEAD + " ");
                commandSB.Append(command.Replace("ID", destination) + " ");
                //Calculate the check sum with all the message so far minus the starting SOH
                commandSB.Append(CalculateCheckCode(commandSB.ToString().Replace(HEAD, "")));

                //Remove spaces from string
                if (stripSpaces) commandSB.Replace(" ", "");

                return commandSB.ToString();
            }
            
            return "";
        }

        /// <summary>
        /// Calculates the check code for the message
        /// </summary>
        /// <param name="messageBody">The message body from the reserved byte (2nd byte), to the ETX inclusive</param>
        /// <returns>String representing the check code</returns>
        protected override string CalculateCheckCode(string messageBody)
        {
            int currentResult = 0;

            messageBody = messageBody.Replace(" ", "");

            for (int i = 0; i < messageBody.Length; i += 2)
            {
                try
                {
                    string hByte = messageBody.Substring(i, 2);
                    int iByte = Convert.ToInt32(hByte, 16);
                    currentResult += iByte;
                }
                catch (Exception e) {
                    throw new Exception("Error calculating checkcode: " + e.Message);
                }
            }

            String result = currentResult.ToString("X");
            if (result.Length >= 3)
            {
                return result.Substring(result.Length - 2, 2);
            }
            else if (result.Length == 2)
            {
                return result;
            }
            else
            {
                return "0" + result;
            }
        }

        public override CommandResponse BuildResponse(byte[] buff)
        {
            CommandResponse response = new CommandResponse();
            try {
                List<String> hexBytes = new List<String>();
                foreach (byte b in buff)
                {
                    string hexByte = b.ToString("X");
                    if (hexByte.Length == 1) hexByte = "0" + hexByte;
                    hexBytes.Add(hexByte);
                }

                //Success
                if (hexBytes[4] == "41")
                {
                    response.success = true;
                }
                else if (hexBytes[4] == "4E")
                {
                    response.success = false;
                }
                else if (hexBytes[0] == "00")
                {
                    response.success = true;
                    response.data = "Sent to all, no response expected";
                    return response;
                }
                else
                {
                    throw new Exception("Invalid ACK/NACK byte");
                }

                if (response.success)
                {
                    response.errorCode = -2;

                    string data = "";
                    bool first = true;
                    for (int i = 6; i <= hexBytes.Count - 2; i++){
                        if (!first)
                        {
                            data += "-";
                        }
                        first = false;
                        data += hexBytes[i];
                    }
                    response.data = data;
                }
                else
                {
                    response.errorCode = int.Parse(hexBytes[6], System.Globalization.NumberStyles.HexNumber);
                }
            } catch (Exception e){
                response.success = false;
                response.errorCode = -1;
            }

            return response;
        }

        public override CommandResponse BuildResponseNew(byte[] buff)
        {
            CommandResponse response = new CommandResponse();
            try
            {
                List<String> hexBytes = new List<String>();
                foreach (byte b in buff)
                {
                    string hexByte = b.ToString("X");
                    if (hexByte.Length == 1) hexByte = "0" + hexByte;
                    hexBytes.Add(hexByte);
                }

                //Success
                if (hexBytes[4] == "41")
                {
                    response.success = true;
                }
                else if (hexBytes[4] == "4E")
                {
                    response.success = false;
                }
                else if (hexBytes[0] == "00")
                {
                    response.success = true;
                    response.data = "Sent to all, no response expected";
                    return response;
                }
                else
                {
                    throw new Exception("Invalid ACK/NACK byte");
                }

                if (response.success)
                {
                    response.errorCode = -2;

                    string data = "";
                    bool first = true;
                    for (int i = 6; i <= hexBytes.Count - 2; i++)
                    {
                        if (!first)
                        {
                            data += "-";
                        }
                        first = false;
                        data += hexBytes[i];
                    }
                    response.data = data;
                }
                else
                {
                    response.errorCode = int.Parse(hexBytes[6], System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch (Exception e)
            {
                response.success = false;
                response.errorCode = -1;
            }

            return response;
        }

        public override void BuildParamsData(List<string> args)
        {
            paramsData = "";
            for (int i = 0; i < args.Count; i++)
            {
                int dec = Convert.ToInt32(args[i]);
                string hexVal = Calculations.DecToHex(dec);
                if(hexVal.Length % 2 != 0)
                {
                    //String is off odd length. Add '0' as a prefix.
                    hexVal = "0" + hexVal;
                }
                paramsData = paramsData + hexVal;
            }
        }

        public override string BuildCommandWithParamsData(string currentMessage, string paramsData)
        {
            string commandFull = currentMessage + paramsData;
            return commandFull;
        }

        public override void InitRSData()
        {

        }

        public override List<RSFormat> GetRSData()
        {
            return new List<RSFormat>();
        }

        public override string GetParamsData()
        {
            return paramsData;
        }
    }
}