﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RS232_Display_Controller.COMController;
using System.IO.Ports;
using RS232_Display_Controller.Classes;

namespace RS232_Display_Controller.CommandBuilder
{
    public class EspritScreenCommandBuilder : ScreenCommandBuilder
    {

        private static readonly SerialPortSpec portSpec = new SerialPortSpec(9600, Parity.None, 8, StopBits.One, Handshake.None);

        private string paramsData = "";

        public EspritScreenCommandBuilder()
        {
            //Set up commands for this builder
            //Any instances of STX will be replaced by the constant above later
            commandValues.Add(COMController.ComController.Commands.POWER_OFF, "b0");
            commandValues.Add(COMController.ComController.Commands.POWER_ON, "b1");
        }

        public override SerialPortSpec GetSerialPortSpec()
        {
            return portSpec;
        }

        public override string BuildCommand(string command, string destinationId, bool stripSpaces)
        {
            return command;
        }

        public override string BuildCommandNew(string command, string destinationId, bool stripSpaces)
        {
            return command;
        }

        public override string BuildCommand(COMController.ComController.Commands command, string destinationId, bool stripSpaces)
        {
            String commandValue = null;

            bool validCommand = commandValues.TryGetValue(command, out commandValue);

            if (validCommand)
            {
                return commandValue;
            }
            else
            {
                throw new InvalidOperationException("This command is not available on this device");
            }
        }

        /// <summary>
        /// Calculates the check code for the message
        /// </summary>
        /// <param name="messageBody">The message body from the reserved byte (2nd byte), to the ETX inclusive</param>
        /// <returns>String representing the check code</returns>
        protected override string CalculateCheckCode(string messageBody)
        {
            return "";
        }

        public override CommandResponse BuildResponse(byte[] buff)
        {
            CommandResponse response = new CommandResponse();
            response.success = true;

            response.dataBytes = buff;
            
            List<String> hexBytes = new List<String>();
            foreach (byte b in buff)
            {
                //hexBytes.Add((b.ToString("{0:x2}")));
                hexBytes.Add(Convert.ToChar(b).ToString());
            }
            
            string data = "";
            for (int i = 0; i < hexBytes.Count - 1; i++)
            {
                data += hexBytes[i];
            }
            response.data = data;

            return response;
        }

        public override CommandResponse BuildResponseNew(byte[] buff)
        {
            CommandResponse response = new CommandResponse();
            response.success = true;

            response.dataBytes = buff;

            List<String> hexBytes = new List<String>();
            foreach (byte b in buff)
            {
                //hexBytes.Add((b.ToString("{0:x2}")));
                hexBytes.Add(Convert.ToChar(b).ToString());
            }

            string data = "";
            for (int i = 0; i < hexBytes.Count - 1; i++)
            {
                data += hexBytes[i];
            }
            response.data = data;

            return response;
        }

        public override void BuildParamsData(List<string> args)
        {
            
        }

        public override string BuildCommandWithParamsData(string currentMessage, string paramsData)
        {
            string message = currentMessage;
            return message;
        }

        public override void InitRSData()
        {

        }

        public override List<RSFormat> GetRSData()
        {
            return new List<RSFormat>();
        }

        public override string GetParamsData()
        {
            return paramsData;
        }

    }
}
