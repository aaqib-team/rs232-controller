﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace RS232_Display_Controller.CommandBuilder
{
    public class SerialPortSpec
    {
        public int BaudRate {get; set;}
        public Parity Parity { get; set; }
        public int DataBits { get; set; }
        public StopBits StopBits { get; set; }
        public Handshake FlowControl { get; set; }

        public SerialPortSpec(int baudRate, Parity parity, int dataBits, StopBits stopBits, Handshake flowControl)
        {
            this.BaudRate = baudRate;
            this.Parity = parity;
            this.DataBits = dataBits;
            this.StopBits = stopBits;
            this.FlowControl = flowControl;
        }
    }
}
