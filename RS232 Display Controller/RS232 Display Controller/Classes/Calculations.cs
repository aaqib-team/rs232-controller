﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RS232_Display_Controller.Classes
{
    class Calculations
    {

        public static string ByteArrayToHexaString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static string ConvertStringToHex(string asciiString)
        {
            string hex = "";
            foreach (char c in asciiString)
            {
                int tmp = c;
                hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
            }
            return hex;
        }

        public static string DecToHex(int val)
        {
            return val.ToString("X");
            //return Convert.ToString(val,16);
        }

        public static List<string> GetChunks(string value, int chunkSize)
        {
            List<string> triplets = new List<string>();
            try
            {
                for (int i = 0; i < value.Length; i += chunkSize)
                    if (i + chunkSize > value.Length)
                        triplets.Add(value.Substring(i));
                    else
                        triplets.Add(value.Substring(i, chunkSize));
            }
            catch (Exception e)
            {
                //Program.Controller.Log("GetChunks. Exception: " + e.Message, LOG_TYPE.ERROR);
            }
            return triplets;
        }

    }
}
