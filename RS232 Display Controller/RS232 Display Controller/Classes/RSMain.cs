﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RS232_Display_Controller.Classes
{
    public class RSMain
    {

        public static string RSCommandsDirectory = @"C:\RS232\conf\";
        public static string RSCommandsFile = @"rscommands.json";
        public static string RSCommandFileFullPath = RSCommandsDirectory + RSCommandsFile;

        /// <summary>
        /// Serializes Object Classes (Array of Classes) to JSON string format that can be written to a text file.
        /// </summary>
        public static string SerializeJSONToString(object value)
        {
            return JsonConvert.SerializeObject(value);
        }

        /// <summary>
        /// Deserialize Specified File (Json) into Database object type (Derived).
        /// </summary>
        /// <returns></returns>
        public static List<RSFormat> DeserializeJSONString(string jsonData, List<RSFormat> rsData)
        {
            try
            {
                rsData.Clear();
                rsData= JsonConvert.DeserializeObject<List<RSFormat>>(jsonData);
                return rsData;
            }
            catch (Exception e)
            {
            }
            return new List<RSFormat>();
        }

        /// <summary>
        /// Get Data from specified file.
        /// </summary>
        /// <returns></returns>
        public static string GetDataFromFile()
        {
            //base.GetDataFromFile(filename);
            try
            {
                if (!File.Exists(RSCommandFileFullPath)) //File don't exists. Then create sub/directories & file.
                {
                    Directory.CreateDirectory(RSCommandsDirectory); //Create directory and sub-directories.
                    File.Create(RSCommandFileFullPath).Close();
                }

                if (RSCommandFileFullPath != null)
                {
                    String data = File.ReadAllText(RSCommandFileFullPath);
                    return data;
                }
            }
            catch (Exception e)
            {
                //Log.I(GetType(), "Error, retrieving data from file, Exception: " + e.Message);
            }
            return "";
        }

        /// <summary>
        /// Update Data File given a json array.
        /// </summary>
        /// <param name="FilePath"></param>
        /// <param name="JsonToFile">Dictionary Array </param>
        public static void WriteDataToFile(string jsonToFile)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(RSCommandFileFullPath, false))
                {
                    try
                    {
                        writer.Write(jsonToFile);
                    }
                    catch (Exception e)
                    {
                        //Log.I(GetType(), "Unable to Write to File 1: " + e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                //Log.I(GetType(), "Unable to Write to File 2: " + e.Message);
            }
        }

        /// <summary>
        /// Convert Data to RSFormat type.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="rsData"></param>
        public static void ConvertDataToRSFormat(string data, List<RSFormat> rsData)
        {

            rsData.Clear();
            JObject jObj = (JObject)JsonConvert.DeserializeObject(data);
            foreach (var p in jObj)
            {              

                JObject jObj2 = (JObject)JsonConvert.DeserializeObject(p.Value.ToString());

                RSFormat rsF = new RSFormat();

                rsF.displayName = jObj2["displayName"].ToString();
                rsF.commandName = jObj2["commandName"].ToString();
                rsF.unityCommand = jObj2["unityCommand"].ToString();
                rsF.operation = jObj2["operation"].ToString();
                rsF.message = jObj2["message"].ToString();
                try
                {
                    rsF.fullOrMessage = jObj2["FullorMessageCommand"].ToString();
                }
                catch(Exception e)
                {
                    
                }
                rsData.Add(rsF);
            }
        }

        public static string GetRSValueFromKey(string command, List<RSFormat> rsData, string type)
        {
            foreach(var rs in rsData)
            {

                if (rs.unityCommand == command)
                {
                    switch (type)
                    {
                        case "displayName":
                            return rs.displayName;

                        case "commandName":
                            return rs.commandName;

                        case "unityCommand":
                            return rs.unityCommand;

                        case "operation":
                            return rs.operation;

                        case "message":
                            return rs.message;

                        case "FullorMessageCommand":
                            return rs.fullOrMessage;

                        default:
                            break;
                    }
                }
            }

            return "";
        }


    }
}
