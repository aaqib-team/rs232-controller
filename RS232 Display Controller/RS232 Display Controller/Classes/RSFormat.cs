﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RS232_Display_Controller.Classes
{
    /// <summary>
    /// Format for RS232 Commands.
    /// </summary>
    public class RSFormat
    { 

        /// <summary>
        /// Display Name. e.g NEC
        /// </summary>
        public string displayName
        {
            get;
            set;
        }

        /// <summary>
        /// Command Name e.g Power On
        /// </summary>
        public string commandName
        {
            get;
            set;
        }

        /// <summary>
        /// Unity / Socket command. e.g power-on
        /// </summary>
        public string unityCommand
        {
            get;
            set;
        }

        /// <summary>
        /// Read / Write Operation.
        /// </summary>
        public string operation
        {
            get;
            set;
        }

        /// <summary>
        /// Command Message.
        /// </summary>
        public string message
        {
            get;
            set;
        }

        /// <summary>
        /// Command Full or Message.
        /// </summary>
        public string fullOrMessage
        {
            get;
            set;
        }

    }
}
